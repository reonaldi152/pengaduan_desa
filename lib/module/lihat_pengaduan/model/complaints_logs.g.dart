// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'complaints_logs.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ComplaintsLogs _$ComplaintsLogsFromJson(Map<String, dynamic> json) =>
    ComplaintsLogs(
      row: (json['row'] as List<dynamic>?)
          ?.map((e) => Rows.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ComplaintsLogsToJson(ComplaintsLogs instance) =>
    <String, dynamic>{
      'row': instance.row,
    };
