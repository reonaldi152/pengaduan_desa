// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rows_cmpLogs.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Rows _$RowsFromJson(Map<String, dynamic> json) => Rows(
      id: json['id'],
      complaintId: json['complaint_id'],
      status: json['status'],
      note: json['note'],
      createdAt: json['created_at'],
      createdBy: json['created_by'],
      code: json['code'],
      name: json['name'],
      email: json['email'],
      address: json['address'],
      content: json['content'],
      attachment: json['attachment'],
      updatedAt: json['updated_at'],
      deletedAt: json['deleted_at'],
    );

Map<String, dynamic> _$RowsToJson(Rows instance) => <String, dynamic>{
      'id': instance.id,
      'complaint_id': instance.complaintId,
      'status': instance.status,
      'note': instance.note,
      'created_at': instance.createdAt,
      'created_by': instance.createdBy,
      'code': instance.code,
      'name': instance.name,
      'email': instance.email,
      'address': instance.address,
      'content': instance.content,
      'attachment': instance.attachment,
      'updated_at': instance.updatedAt,
      'deleted_at': instance.deletedAt,
    };
