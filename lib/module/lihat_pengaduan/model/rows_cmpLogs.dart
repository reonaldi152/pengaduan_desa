import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'rows_cmpLogs.g.dart';

@JsonSerializable()
class Rows extends Equatable {
  const Rows({
    this.id,
    this.complaintId,
    this.status,
    this.note,
    this.createdAt,
    this.createdBy,
    this.code,
    this.name,
    this.email,
    this.address,
    this.content,
    this.attachment,
    this.updatedAt,
    this.deletedAt,
  });

  factory Rows.fromJson(Map<String, dynamic> json) =>
      _$RowsFromJson(json);

  Map<String, dynamic> toJson() => _$RowsToJson(this);

  final dynamic id;
  @JsonKey(name: 'complaint_id')
  final dynamic complaintId;
  final dynamic status;
  final dynamic note;
  @JsonKey(name: 'created_at')
  final dynamic createdAt;
  @JsonKey(name: 'created_by')
  final dynamic createdBy;
  final dynamic code;
  final dynamic name;
  final dynamic email;
  final dynamic address;
  final dynamic content;
  final dynamic attachment;
  @JsonKey(name: 'updated_at')
  final dynamic updatedAt;
  @JsonKey(name: 'deleted_at')
  final dynamic deletedAt;

  @override
  List<Object?> get props =>
      throw [complaintId, note, code, name, email, address, content];
}
