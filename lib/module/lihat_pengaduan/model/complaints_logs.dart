import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:pengaduan_desa/module/lihat_pengaduan/model/rows_cmpLogs.dart';

part 'complaints_logs.g.dart';

@JsonSerializable()
class ComplaintsLogs extends Equatable {
  const ComplaintsLogs({
    this.row
  });

  factory ComplaintsLogs.fromJson(Map<String, dynamic> json) => _$ComplaintsLogsFromJson(json);

  Map<String, dynamic> toJson() => _$ComplaintsLogsToJson(this);

  final List<Rows>? row;

  @override
  List<Object?> get props =>
      throw [];
}
