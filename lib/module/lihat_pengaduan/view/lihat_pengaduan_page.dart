import 'package:flutter/material.dart';
import 'package:pengaduan_desa/config/app_color.dart';
import 'package:pengaduan_desa/module/complaint_viewmodel.dart';
import 'package:pengaduan_desa/module/lihat_pengaduan/model/complaints_logs.dart';
import 'package:pengaduan_desa/widget/textformfield_outline.dart';

class LihatPengaduanPage extends StatefulWidget {
  const LihatPengaduanPage({Key? key}) : super(key: key);

  @override
  State<LihatPengaduanPage> createState() => _LihatPengaduanPageState();
}

class _LihatPengaduanPageState extends State<LihatPengaduanPage> {

  final TextEditingController _searchController = TextEditingController();

  List<Step> getSteps() {
    List<Step> mySteps = [];
    for(int i = 0; i< 5; i++) {
      final _step = Step(title: Text('ayo'), content: Container(), isActive: true);
      mySteps.add(_step);
    }
    return mySteps;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Lihat Pengaduan"),
        centerTitle: true,
        backgroundColor: AppColor.white,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 16),
              child: TextFormFieldOutline(
                title: "Kode Pengaduan",
                titleStyle: const TextStyle(
                    color: AppColor.title,
                    fontSize: 12,
                    fontWeight: FontWeight.w700),
                hintText: "masukkan kode pengaduan",
                hintStyle: const TextStyle(
                    color: AppColor.hint1,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
                textStyle: const TextStyle(
                    color: AppColor.title,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
                controller: _searchController,
                keyboardType: TextInputType.name,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Wajib masukkan kode pengaduan';
                  }
                  return null;
                },
              ),
            ),
            // ListView.builder(
            //   shrinkWrap: true,
            //   itemBuilder: (context, index) {
            //     return Stepper(
            //       physics: ClampingScrollPhysics(),
            //       steps: <Step>[
            //         Step(
            //           title: const Text('Step 1 title'),
            //           content: Container(
            //               alignment: Alignment.centerLeft,
            //               child: const Text('Content for Step 1')),
            //         ),
            //         const Step(
            //           title: Text('Step 2 title'),
            //           content: Text('Content for Step 2'),
            //         ),
            //       ],
            //     );
            //   },
            // ),
            const SizedBox(height: 30),
            // Stepper(steps: <Step>[
            //   Step(
            //
            //     title: const Text('Step 1 title'),
            //     content: Container(
            //         alignment: Alignment.centerLeft,
            //         child: const Text('Content for Step 1')),
            //   ),
            //   const Step(
            //     title: Text('Step 2 title'),
            //     content: Text('Content for Step 2'),
            //   ),
            // ]),
            Stepper(steps: getSteps()),
          ],
        ),
      ),
    );
  }

  ComplaintsLogs? _complaintsLogs;

  getPengaduan() async {
    ComplaintViewmodel().lihatPengaduan(code: _searchController.text).then((value) {
      if (value.code == 200){
        ComplaintsLogs? complaintsLogs = ComplaintsLogs.fromJson(value.response);
        setState(() {
          _complaintsLogs = complaintsLogs;
        });
      }
    });
  }
}
