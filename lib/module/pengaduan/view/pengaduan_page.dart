import 'dart:math';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pengaduan_desa/config/app_color.dart';
import 'package:pengaduan_desa/module/complaint_viewmodel.dart';
import 'package:pengaduan_desa/module/lihat_pengaduan/model/complaints_logs.dart';
import 'package:pengaduan_desa/module/lihat_pengaduan/view/lihat_pengaduan_page.dart';
import 'package:pengaduan_desa/module/pengaduan/model/complaints.dart';
import 'package:pengaduan_desa/module/success%20page/success_page.dart';
import 'package:pengaduan_desa/widget/textformfield_outline.dart';

class PengaduanPage extends StatefulWidget {
  const PengaduanPage({Key? key}) : super(key: key);

  @override
  State<PengaduanPage> createState() => _PengaduanPageState();
}

class _PengaduanPageState extends State<PengaduanPage> {
  final _formKey = GlobalKey<FormState>();


  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _codeController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _addressController = TextEditingController();
  final TextEditingController _contentController = TextEditingController();

  @override
  void initState() {
    super.initState();
    getPengaduan();
  }

  @override
  Widget build(BuildContext context) {

    Random random = new Random();
    int randomNumber = random.nextInt(10000);

    return Scaffold(
      backgroundColor: AppColor.white,
      appBar: AppBar(
        title: const Text("Pengaduan"),
        centerTitle: true,
        backgroundColor: AppColor.white,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: MediaQuery.of(context).padding.top + 20,
                ),
                const SizedBox(height: 20),
                const Text(
                  'Isi Form Untuk Pengaduan',
                  style: TextStyle(
                    color: AppColor.title,
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                SizedBox(height: 40),
                TextFormFieldOutline(
                  enabled: false,
                  title: "Kode Pengaduan",
                  titleStyle: const TextStyle(
                      color: AppColor.title,
                      fontSize: 12,
                      fontWeight: FontWeight.w700),
                  hintText: "Cth. John Doe",
                  hintStyle: const TextStyle(
                      color: AppColor.hint1,
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                  textStyle: const TextStyle(
                      color: AppColor.title,
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                  controller: TextEditingController()..text = (_complaintsLogs?.row?.length != null) ? "P/2023/000${_complaintsLogs!.row!.length + 33}" : "loading",
                  keyboardType: TextInputType.name,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Nama harus di isi';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 5),
                TextFormFieldOutline(
                  title: "Nama Lengkap",
                  titleStyle: const TextStyle(
                      color: AppColor.title,
                      fontSize: 12,
                      fontWeight: FontWeight.w700),
                  hintText: "Cth. John Doe",
                  hintStyle: const TextStyle(
                      color: AppColor.hint1,
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                  textStyle: const TextStyle(
                      color: AppColor.title,
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                  controller: _nameController,
                  keyboardType: TextInputType.name,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Nama harus di isi';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 5),
                TextFormFieldOutline(
                  title: "No. HP",
                  titleStyle: const TextStyle(
                      color: AppColor.title,
                      fontSize: 12,
                      fontWeight: FontWeight.w700),
                  hintText: "081234567",
                  hintStyle: const TextStyle(
                      color: AppColor.hint1,
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                  textStyle: const TextStyle(
                      color: AppColor.title,
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                  controller: _phoneController,
                  keyboardType: TextInputType.name,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Nomor HP harus diisi';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 5),
                TextFormFieldOutline(
                  title: "Email",
                  titleStyle: const TextStyle(
                      color: AppColor.title,
                      fontSize: 12,
                      fontWeight: FontWeight.w700),
                  hintText: "email@gmail.com",
                  hintStyle: const TextStyle(
                      color: AppColor.hint1,
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                  textStyle: const TextStyle(
                      color: AppColor.title,
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                  controller: _emailController,
                  keyboardType: TextInputType.name,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Email harus diisi';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 5),
                TextFormFieldOutline(
                  title: "Alamat",
                  titleStyle: const TextStyle(
                      color: AppColor.title,
                      fontSize: 12,
                      fontWeight: FontWeight.w700),
                  hintStyle: const TextStyle(
                      color: AppColor.hint1,
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                  textStyle: const TextStyle(
                      color: AppColor.title,
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                  controller: _addressController,
                  keyboardType: TextInputType.name,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Alamat harus diisi';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 5),
                TextFormFieldOutline(
                  title: "Isi Pengaduan",
                  titleStyle: const TextStyle(
                      color: AppColor.title,
                      fontSize: 12,
                      fontWeight: FontWeight.w700),
                  hintStyle: const TextStyle(
                      color: AppColor.hint1,
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                  textStyle: const TextStyle(
                      color: AppColor.title,
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                  controller: _contentController,
                  keyboardType: TextInputType.name,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Harus diisi';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 20),
                Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 30),
                  child: SizedBox(
                    width: double.infinity,
                    height: 36,
                    child: TextButton(
                      onPressed: () {
                        // postPengaduan();
                        _nameController.clear();
                        _emailController.clear();
                        _phoneController.clear();
                        _addressController.clear();
                        _contentController.clear();
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SuccessPage(kodePengaduan: "P/2023/0000${_complaintsLogs!.row!.length + 33}")));
                      },
                      style: OutlinedButton.styleFrom(
                          backgroundColor: AppColor.colorPrimary,
                          padding: const EdgeInsets.symmetric(vertical: 4),
                          shape: const RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(24.0)),
                          )),
                      child: const Text(
                        "Kirim Pengaduan",
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 13,
                            color: AppColor.white),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Complaints? _complaints;
  postPengaduan() async {
    ComplaintViewmodel()
        .pengaduan(
      code: "P/2023/000${_complaintsLogs!.row!.length + 50}",
      name: _nameController.text,
      address: _addressController.text,
      content: _contentController.text,
      email: _emailController.text,
      phone: _phoneController.text,
    )
        .then((value) {
      if (value.code == 200) {
        Complaints? complaints = Complaints.fromJson(value.response);
        setState(() {
          _complaints = complaints;
        });
        Fluttertoast.showToast(msg: complaints.message);
      }
    });
  }

  ComplaintsLogs? _complaintsLogs;

  getPengaduan() async {
    ComplaintViewmodel().lihatPengaduan().then((value) {
      if (value.code == 200){
        ComplaintsLogs? complaintsLogs = ComplaintsLogs.fromJson(value.response);
        setState(() {
          _complaintsLogs = complaintsLogs;
        });
      }
    });
  }
}
