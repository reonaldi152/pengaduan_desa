import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'complaints.g.dart';

@JsonSerializable()
class Complaints extends Equatable {
  const Complaints({this.message});
  factory Complaints.fromJson(Map<String, dynamic> json) =>
      _$ComplaintsFromJson(json);

  Map<String, dynamic> toJson() => _$ComplaintsToJson(this);

  final dynamic message;

  @override
  List<Object?> get props => [message];
}
