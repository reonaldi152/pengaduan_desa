import 'package:dio/dio.dart';
import 'package:pengaduan_desa/config/endpoint.dart';
import 'package:pengaduan_desa/config/model/resp.dart';
import 'package:pengaduan_desa/config/network.dart';

class ComplaintViewmodel {
  Future<Resp> pengaduan({code, name, phone, email, address, content}) async {
    FormData formData = FormData.fromMap({
      "code": code,
      "name": name,
      "phone": phone,
      "email": email,
      "address": address,
      "content": content,
    });

    var resp = await Network.postApi(Endpoint.pengaduan, formData);

    Resp data = Resp.fromJson(resp);
    return data;
  }

  Future<Resp> lihatPengaduan({code}) async {

    dynamic resp;

    if (code == null){
      resp = await Network.getApi("https://permata-agroindustri.com/pengaduan/complaints.php");
    } else {
      resp = await Network.getApi("https://permata-agroindustri.com/pengaduan/complaints.php?code=$code");
    }

    Resp data = Resp.fromJson(resp);
    return data;
  }
}
