import 'package:flutter/material.dart';
import 'package:pengaduan_desa/config/app_color.dart';
import 'package:pengaduan_desa/main.dart';
import 'package:pengaduan_desa/module/lihat_pengaduan/view/lihat_pengaduan_page.dart';
import 'package:pengaduan_desa/module/pengaduan/view/pengaduan_page.dart';
import 'package:pengaduan_desa/widget/app_bar.dart';

class SuccessPage extends StatefulWidget {
  final dynamic kodePengaduan;
  const SuccessPage({Key? key, this.kodePengaduan}) : super(key: key);

  @override
  State<SuccessPage> createState() => _SuccessPageState();
}

class _SuccessPageState extends State<SuccessPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Container(
              color: AppColor.white,
              height: 56,
              padding: const EdgeInsets.symmetric(horizontal: 4),
              child: Row(
                children: [
                  Expanded(
                      flex: 1,
                      child: IconButton(
                          onPressed: () =>  Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const PengaduanPage())),
                          icon: const Icon(Icons.arrow_back_ios_sharp))),
                  const Expanded(
                      flex: 8,
                      child: Center(
                        child: Text(
                          "Sukses Pengaduan",
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                              color: AppColor.title),
                        ),
                      )),
                ],
              ),
            ),
            const SizedBox(height: 50),
            const Text(
              'Pengaduan\nBerhasil Dikirim',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w800,
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 26,
            ),
            Text(
              'Untuk melihat status pengaduan\ncari pengaduan anda\ndengan kode pengaduan : ${widget.kodePengaduan}',
              style: TextStyle(
                fontSize: 16,
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 30,
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 30),
              child: SizedBox(
                width: double.infinity,
                height: 36,
                child: TextButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const LihatPengaduanPage()));
                  },
                  style: OutlinedButton.styleFrom(
                      backgroundColor: AppColor.colorPrimary,
                      padding: const EdgeInsets.symmetric(vertical: 4),
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(24.0)),
                      )),
                  child: const Text(
                    "Lihat Pengaduan",
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 13,
                        color: AppColor.white),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
