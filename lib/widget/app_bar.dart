import 'package:pengaduan_desa/config/app_color.dart';
import 'package:flutter/material.dart';

class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
  const MyAppBar({Key? key, required this.title, this.isCloseShowing})
      : super(key: key);

  final String title;
  final bool? isCloseShowing;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColor.white,
      height: 56,
      padding: const EdgeInsets.symmetric(horizontal: 4),
      child: Row(
        children: [
          isCloseShowing == true
              ? Container()
              : Expanded(
                  flex: 1,
                  child: IconButton(
                      onPressed: () => Navigator.pop(context),
                      icon: const Icon(Icons.arrow_back_ios_sharp))),
          Expanded(
              flex: 8,
              child: Center(
                child: Text(
                  title,
                  style: const TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                      color: AppColor.title),
                ),
              )),
          isCloseShowing == true
              ? IconButton(
                  onPressed: () => Navigator.pop(context),
                  icon: const Icon(Icons.close, color: AppColor.title))
              : Expanded(flex: 1, child: Container()),
        ],
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => throw UnimplementedError();
}
