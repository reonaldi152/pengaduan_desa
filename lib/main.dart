import 'package:flutter/material.dart';

import 'config/app_color.dart';
import 'config/routes/routes.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          fontFamily: "Euclid Circular A",
          hintColor: AppColor.hint2,
          primaryColor: AppColor.colorPrimary,
          canvasColor: AppColor.white,
          primarySwatch: Colors.grey,
          backgroundColor: AppColor.white,
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}
