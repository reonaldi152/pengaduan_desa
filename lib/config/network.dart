import 'package:flutter/cupertino.dart';

import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'network/dio_exception.dart';
import 'network/interceptors/authorization_interceptor.dart';
import 'network/interceptors/logger_interceptor.dart';

class Network {
  static Future<dynamic> postApi(String url, dynamic formData) async {
    try {
      var dio = Dio(
        BaseOptions(
          baseUrl: 'https://permata-agroindustri.com/pengaduan',
          connectTimeout: 500000,
          receiveTimeout: 300000,
          responseType: ResponseType.json,
          maxRedirects: 0,
        ),
      )..interceptors.addAll([
        AuthorizationInterceptor(),
        LoggerInterceptor(),
      ]);
      Response rest = await dio.post(url, data: formData);
      // debugPrint("$url response:${rest.data??""}");
      dio.close();
      return rest.data;
    } on DioError catch (err) {
      final errorMessage = DioException.fromDioError(err).toString();
      debugPrint(errorMessage.toString());
      throw errorMessage;
    } catch (e) {
      debugPrint(e.toString());
      throw e.toString();
    }
  }

  static Future<dynamic> postApiWithHeaders(String url, body, Map<String, dynamic> header) async {
    try {
      var dio = Dio(
        BaseOptions(
          baseUrl: 'https://permata-agroindustri.com/pengaduan',
          connectTimeout: 500000,
          receiveTimeout: 300000,
          responseType: ResponseType.json,
          maxRedirects: 0,
        ),
      )..interceptors.addAll([
        // AuthorizationInterceptor(),
        LoggerInterceptor(),
      ]);
      Response restValue = await dio.post(url, data: body, options: Options(headers: header));
      debugPrint("$url response:${restValue.data??""}");
      dio.close();
      header.clear();
      return restValue.data;
    } on DioError catch (err) {
      final errorMessage = DioException.fromDioError(err).toString();
      throw errorMessage;
    } catch (e) {
      debugPrint(e.toString());
      throw e.toString();
    }
  }

  static Future<dynamic> postApiWithHeadersWithoutData(String url, Map<String, dynamic> header) async {
    try {
      var dio = Dio(
        BaseOptions(
          baseUrl: 'https://permata-agroindustri.com/pengaduan',
          connectTimeout: 500000,
          receiveTimeout: 300000,
          responseType: ResponseType.json,
          maxRedirects: 0,
        ),
      )..interceptors.addAll([
        AuthorizationInterceptor(),
        LoggerInterceptor(),
      ]);
      Response restValue = await dio.post(url, options: Options(headers: header));
      debugPrint("$url response:${restValue.data??""}");
      dio.close();
      header.clear();
      return restValue.data;
    } on DioError catch (err) {
      final errorMessage = DioException.fromDioError(err).toString();
      throw errorMessage;
    } catch (e) {
      debugPrint(e.toString());
      throw e.toString();
    }
  }

  static Future<dynamic> postApiWithHeadersAndContentType(String url, Map<String, String> body, {String? contentType, Map<String, dynamic>? header}) async {
    try {
      var dio = Dio(
        BaseOptions(
            baseUrl: 'https://permata-agroindustri.com/pengaduan',
            connectTimeout: 500000,
            receiveTimeout: 300000,
            responseType: ResponseType.json,
            maxRedirects: 0,
            contentType: 'application/x-www-form-urlencoded'
        ),
      )..interceptors.addAll([
        AuthorizationInterceptor(),
        LoggerInterceptor(),
      ]);
      Response restValue = await dio.post(url, data: body);
      debugPrint("$url response:${restValue.data??""}");
      dio.close();
      header?.clear();
      return restValue.data;
    } on DioError catch (err) {
      debugPrint(err.message);
      debugPrint(err.response.toString());
      debugPrint(err.type.toString());
      debugPrint(err.requestOptions.toString());
      debugPrint(err.error);
      return err.response?.data;
    } catch (e) {
      debugPrint(e.toString());
      throw e.toString();
    }
  }

  static Future<dynamic> getApi(String url, {String? baseurl}) async {
    try {
      var dio = Dio(
        BaseOptions(
          baseUrl: 'https://permata-agroindustri.com/pengaduan',
          connectTimeout: 500000,
          receiveTimeout: 300000,
          responseType: ResponseType.json,
          maxRedirects: 0,
        ),
      )..interceptors.addAll([
        AuthorizationInterceptor(),
        LoggerInterceptor(),
      ]);
      Response restGet = await dio.get(url);
      debugPrint("$url response:${restGet.data??""}");
      dio.close();
      return restGet.data;
    } on DioError catch (err) {
      final errorMessage = DioException.fromDioError(err).toString();
      throw errorMessage;
    } catch (e) {
      debugPrint(e.toString());
      throw e.toString();
    }
  }

  static Future<dynamic> getApiWithHeaders(String url, Map<String, dynamic> header) async {
    try {
      var dio = Dio(
        BaseOptions(
          baseUrl: 'https://permata-agroindustri.com/pengaduan',
          connectTimeout: 500000,
          receiveTimeout: 300000,
          responseType: ResponseType.json,
          maxRedirects: 0,
        ),
      )..interceptors.addAll([
        AuthorizationInterceptor(),
        LoggerInterceptor(),
      ]);
      Response restGet = await dio.get(url, options: Options(headers: header));
      debugPrint("$url response:${restGet.data??""}");
      dio.close();
      header.clear();
      return restGet.data;
    } on DioError catch (err) {
      final errorMessage = DioException.fromDioError(err).toString();
      throw errorMessage;
    } catch (e) {
      debugPrint(e.toString());
      throw e.toString();
    }
  }

  static Future<dynamic> putApiWithHeaders(String url, FormData body, Map<String, dynamic> header) async {
    try {
      var dio = Dio(
        BaseOptions(
          baseUrl: 'https://permata-agroindustri.com/pengaduan',
          connectTimeout: 500000,
          receiveTimeout: 300000,
          responseType: ResponseType.json,
          maxRedirects: 0,
        ),
      )..interceptors.addAll([
        AuthorizationInterceptor(),
        LoggerInterceptor(),
      ]);
      Response restValue = await dio.put(url, data: body, options: Options(headers: header));
      debugPrint("$url response:${restValue.data??""}");
      dio.close();
      header.clear();
      return restValue.data;
    } on DioError catch (err) {
      final errorMessage = DioException.fromDioError(err).toString();
      throw errorMessage;
    } catch (e) {
      debugPrint(e.toString());
      throw e.toString();
    }
  }

  static Future<dynamic> putApi(String url, Map<String, dynamic> header) async {
    try {
      var dio = Dio(
        BaseOptions(
          baseUrl: 'https://permata-agroindustri.com/pengaduan',
          connectTimeout: 500000,
          receiveTimeout: 300000,
          responseType: ResponseType.json,
          maxRedirects: 0,
        ),
      )..interceptors.addAll([
        // AuthorizationInterceptor(),
        LoggerInterceptor(),
      ]);
      Response restValue = await dio.put(url, options: Options(headers: header));
      debugPrint("$url response:${restValue.data??""}");
      dio.close();
      header.clear();
      return restValue.data;
    } on DioError catch (err) {
      final errorMessage = DioException.fromDioError(err).toString();
      throw errorMessage;
    } catch (e) {
      debugPrint(e.toString());
      throw e.toString();
    }
  }

}
