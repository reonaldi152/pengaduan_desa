import 'package:flutter/material.dart';

class AppColor {
  static const Color colorPrimary = Color(0xFF2E6CC9);
  static const Color colorPrimary30 = Color(0x4D2E6CC9);
  static const Color colorPrimary0 = Color(0x002E6CC9);
  static const Color colorPrimary10 = Color(0x1A2E6CC9);
  static const Color colorPrimaryBg = Color(0xFFC0D3EF);
  static const Color colorAccent = Color(0xFF4BACE2);
  static const Color colorAccent20 = Color(0x334BACE2);
  static const Color colorCanvas = Colors.transparent;
  static const Color white = Color(0xFFFFFFFF);
  static const Color white30 = Color(0x4DFFFFFF);
  static const Color white50 = Color(0x80FFFFFF);
  static const Color black = Color(0xFF000000);
  static const Color title = Color(0xFF353535);
  static const Color title2 = Color(0xFF10001D);
  static const Color title3 = Color(0xFF454545);
  static const Color subTitle = Color(0xFF959595);
  static const Color hint1 = Color(0xFFC5C5C5);
  static const Color hint2 = Color(0xFFE5E5E5);
  static const Color hintC4 = Color(0xFFC4C4C4);
  static const Color errorLabel = Color(0xFFFF0000);
  static const Color kaspro = Color(0xFF90348D);
  static const Color status = Color(0xFFD4E5FF);
  static const Color lokasi = Color(0xFF6B6B6B);
  static const Color bundle = Color(0xFFFEB127);
  static const Color bundle_bg = Color(0xFFFFE8BF);
  static const Color error = Color(0xFFDC3937);
  static const Color aktif_bg = Color(0xFFCDE1FF);
  static const Color topup = Color(0xFF18A86A);
}
