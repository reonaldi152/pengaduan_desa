// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'resp.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Resp _$RespFromJson(Map<String, dynamic> json) => Resp(
      result: json['result'],
      error: json['error'],
      response: json['response'],
      code: json['code'] as int?,
      data: json['data'],
    );

Map<String, dynamic> _$RespToJson(Resp instance) => <String, dynamic>{
      'code': instance.code,
      'error': instance.error,
      'result': instance.result,
      'response': instance.response,
      'data': instance.data,
    };
