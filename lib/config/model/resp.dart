import 'package:json_annotation/json_annotation.dart';

part 'resp.g.dart';

@JsonSerializable(explicitToJson: true)
class Resp {
  int? code;
  dynamic error;
  dynamic result;
  dynamic response;
  dynamic data;

  Resp({
    this.result,
    this.error,
    this.response,
    this.code,
    this.data
  });

  factory Resp.fromJson(Map<String, dynamic> json) => _$RespFromJson(json);
  Map<String, dynamic> toJson() => _$RespToJson(this);

  @override
  String toString() => 'code = $code, response = $response, result = $result, error = $error';
}